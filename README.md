# Component Manager

This is the "Component Manager" source code from **Using Preact to Time Travel**.

It requires Node 6 to be installed, which comes with npm 3.  Download them at [nodejs.org](https://nodejs.org).


### Installation

```sh
git clone https://bitbucket.org/developit/cms-integration-demo.git
cd cms-integration-demo

# install dependencies:
npm install

# start a dev server:
npm run dev

# build for production:
npm run build

# start a production HTTP/2 server:
NODE_ENV=production npm start
```

