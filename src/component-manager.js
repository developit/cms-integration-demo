import { h, render } from 'preact';


/** Detect CDN path at runtime (used to load chunks) */
let script = document.querySelector('script[src*=component-manager]');
if (script) __webpack_public_path__ = script.src.replace(/\/?[^/]+(\?.*)?$/g, '') + '/'; // eslint-disable-line


/** Load all components from ./components/*, automatically wrapping them in split points. */
export const components = {};
let context = require.context('async!./components', true, /^\.\/[^/]+(\/index)?\.js$/g);
context.keys().forEach( path => {
	let friendlyName = path.replace(/(^\.\/|[/.].+$)/g,'');
	components[friendlyName] = context(path).default;   // contextual require()
});


/** renderComponent('header', { title: 'Hello' }, document.body) */
export function renderComponent(name, props, containerNode) {
	// Render it into the container, diffing against any previous render:
	function update(props={}, first) {
		render(
			h(components[name], props),
			containerNode,
			first ? null : containerNode.lastChild
		);
	}

	update(props, true);
	return { update };
}


/** Search for <div data-component="name"> and render components into each. */
function scan() {
	[].forEach.call(document.querySelectorAll('[data-component]'), container => {
		// Skip if already set up
		if (container.renderer) return;

		// try to grab configuration from props="{\"a\":1}" or JSON in a script tag
		let config = container.getAttribute('data-props') || (container.querySelector('script') || {}).textContent || '{}';

		// render the component
		container.renderer = renderComponent(
			container.getAttribute('data-component'),
			JSON.parse(config.replace(/^\s*\/\/.+$/gm,'')),
			container
		);
	});
}


// Scan immediately on init, then again once document parsing finishes (if it isn't already)
scan();
addEventListener('DOMContentLoaded', scan);
