import GithubStars from '../../components/github-stars';
import style from './style';

// Note: the `repo` prop comes from the URL, courtesy of Router
export default ({ repo }) => (
	<div class={style.repo}>
		<h1>Repo: {repo}</h1>

		<div class={style.starCount}>
			<GithubStars repo={repo} />
		</div>
	</div>
);
