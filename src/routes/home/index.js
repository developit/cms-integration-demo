import { h, Component } from 'preact';
import { route } from 'preact-router';
import style from './style';

export default class Home extends Component {
	search = e => {
		let repo = e.target.value;
		route(`/repo/${repo}`);
	};

	render() {
		return (
			<div class={style.home}>
				<h1>Demo Application</h1>
				<p>This is a demo application.</p>
				<p>Click or search below to view star counts for Github repos:</p>

				<div class={style.search}>
					<input type="search" onSearch={this.search} placeholder="user/repo" />
				</div>
			</div>
		);
	}
}
