import './style/index.less';

import { Router } from 'preact-router';

import Header from './components/header';
import Home from './routes/home';
import Repo from './routes/repo';

export default () => (
	<div id="app">
		<Header />
		<Router>
			<Home path="/" />
			<Repo path="/repo/:repo*" />
		</Router>
	</div>
);
