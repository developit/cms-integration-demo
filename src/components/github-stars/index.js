import { h, Component } from 'preact';
import style from './style';

/** Get the number of stars for a given Github repo:
 *	@returns {Promise<Number>}
 */
function githubStars(repo) {
	return fetch('https://api.github.com/repos/'+repo)
		.then( r => r.json() )
		.then( d => d.stargazers_count );
}

export default class Stars extends Component {
	update(repo) {
		// fetch stars for the repo, then update state to re-render:
		githubStars(repo).then( stars => {
			this.setState({ stars });
		});
	}

	componentDidMount() {
		this.update(this.props.repo);
	}
	
	componentWillReceiveProps(nextProps) {
		if (nextProps.repo!==this.props.repo) {
			this.update(nextProps.repo);
		}
	}

	render({ repo }, { stars='..' }) {
		let url = `//github.com/${repo}`;
		return (
			<a href={url} class={style.stars}>
				⭐️ {stars} Stars
			</a>
		);
	}
}