export default (config, env, helpers) => {
	function removePlugin(name) {
		let plugin = helpers.getPluginsByName(config, name)[0];
		if (plugin) config.plugins.splice(plugin.index, 1);
	}

	if (!env.ssr && process.env.UMD_BUILD) {
		config.entry = './component-manager';
		config.output.library = 'componentManager';
		config.output.libraryTarget = 'umd';
		
		config.output.filename = 'load.js';
		config.output.publicPath = '/component-manager/';
		
		// remove HTML related plugins:
		removePlugin('HtmlWebpackPlugin');
		removePlugin('HtmlWebpackExcludeAssetsPlugin');
		removePlugin('CopyPlugin');
		removePlugin('PushManifestPlugin');
		removePlugin('ScriptExtHtmlWebpackPlugin');
		removePlugin('SWPrecacheWebpackPlugin');
		removePlugin('Object'); // CopyWebpackPlugin
		
		// Disable external CSS
		helpers.getPluginsByName(config, 'ExtractTextPlugin')[0].plugin.options.disable = true;
	}
	return config;
};

